package fr.enssat.kikeou.maze_fraysse.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.maze_fraysse.model.CalendarRepository

/**
 * Créer un view model en y associant un repository
 *
 * @property repository
 */
class MainViewModelFactory(private val repository: CalendarRepository): ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    //factory created to pass repository to view model...
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(repository) as T
    }
}