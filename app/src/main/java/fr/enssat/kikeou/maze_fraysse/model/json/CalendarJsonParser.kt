package fr.enssat.kikeou.maze_fraysse.model.json

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import fr.enssat.kikeou.maze_fraysse.model.Calendar

/**
 * Parser associé à un agenda
 *
 */
class CalendarJsonParser {
    companion object {
        /**
         * Convertit un json en agenda
         *
         * @param json
         * @return Calendar
         */
        fun parseCalendar(json: String): Calendar? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Calendar> = moshi.adapter(Calendar:: class.java)
            return adapter.fromJson(json)
        }

        /**
         * Convertit un agenda en json
         *
         * @param calendar
         * @return String
         */
        fun parseJson(calendar: Calendar): String? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Calendar> = moshi.adapter(Calendar:: class.java)
            return adapter.toJson(calendar)
        }
    }
}