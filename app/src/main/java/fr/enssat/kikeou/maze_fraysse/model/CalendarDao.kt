package fr.enssat.kikeou.maze_fraysse.model

import androidx.room.*
import kotlinx.coroutines.flow.Flow

/**
 * DAO permettant d'effecuter des actions dans une base de données
 *
 */
@Dao
interface CalendarDao {

    /**
     * Récupère tout les agendas de la base de données
     *
     * @return Flow<List<Calendar>>
     */
    @Query("SELECT * FROM calendar_table")
    fun getAll(): Flow<List<Calendar>>

    /**
     * Insère un nouvel agenda dans la base de données
     *
     * @param calendar
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(calendar: Calendar)

    /**
     * Supprime tout les agendas dans la base de données
     *
     */
    @Query("DELETE FROM calendar_table")
    fun deleteAll()

    /**
     * Supprime un agenda dans la base de données
     *
     * @param calendarId Id de l'agenda à supprimer
     */
    @Query("DELETE FROM calendar_table WHERE id = :calendarId")
    fun deleteByCalendarId(calendarId: Int)

    /**
     * Mettre à jour un agenda
     *
     * @param calendar Agenda à mettre à jour
     */
    @Update
    fun updateCalendar(calendar:Calendar)
}