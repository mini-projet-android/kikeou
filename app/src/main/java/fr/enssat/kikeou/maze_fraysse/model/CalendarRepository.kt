package fr.enssat.kikeou.maze_fraysse.model

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

/**
 * Repository qui fait appel au DAO
 *
 * @property calendarDao
 */
class CalendarRepository(private val calendarDao: CalendarDao) {

    val allCalendars: Flow<List<Calendar>> = calendarDao.getAll()

    @WorkerThread
    suspend fun insert(calendar: Calendar){
        calendarDao.insert(calendar)
    }

    @WorkerThread
    suspend fun deleteAll(){
        calendarDao.deleteAll()
    }

    @WorkerThread
    suspend fun delete(idCalendar: Int){
        calendarDao.deleteByCalendarId(idCalendar)
    }

    @WorkerThread
    suspend fun update(calendar: Calendar){
        calendarDao.updateCalendar(calendar)
    }

}