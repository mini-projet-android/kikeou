package fr.enssat.kikeou.maze_fraysse.adapter

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.maze_fraysse.R
import android.content.Intent
import android.provider.MediaStore

/**
 * CalendarViewHolder qui permet l'affichage d'un item dans une recycler view
 *
 * @constructor
 * itemView Vue associer à l'item à afficher
 *
 * @param itemView
 */
class CalendarViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView){
    private lateinit var itemImage: ImageView
    private lateinit var itemText: TextView

    /**
     * Initialisation
     *@param
     *@return
     */
    init {
        itemImage = itemView.findViewById(R.id.imageview)
        itemText = itemView.findViewById(R.id.text)
    }

    /**
     * Spécifie le texte à afficher dans la vue de l'item
     *
     * @param text
     */
    fun bind(text: String?) {
        itemText.text = text
    }

    companion object {
        fun create(parent: ViewGroup): CalendarViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_item, parent, false)
            return CalendarViewHolder(view)
        }
    }
}