package fr.enssat.kikeou.maze_fraysse.main

import androidx.lifecycle.*
import fr.enssat.kikeou.maze_fraysse.model.Calendar
import fr.enssat.kikeou.maze_fraysse.model.CalendarRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

/**
 * View model de l'application. Il permet de gérer l'actualisation des calendriers dans les fragments
 *
 * @property repository
 */
class MainViewModel(private val repository: CalendarRepository) : ViewModel() {
    val calendars = repository.allCalendars.asLiveData()

    private val scope = CoroutineScope(SupervisorJob())

    fun insertCalendar(item: Calendar) = scope.launch {
        repository.insert(item)
    }

    fun deleteAllCalendar() = scope.launch {
        repository.deleteAll()
    }

    fun deleteCalendar(id: Int) = scope.launch {
        repository.delete(id)
    }

    fun updateCalendar(item: Calendar) = scope.launch {
        repository.update(item)
    }

}