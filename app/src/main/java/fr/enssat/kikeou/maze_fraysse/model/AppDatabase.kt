package fr.enssat.kikeou.maze_fraysse.model

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.enssat.kikeou.maze_fraysse.model.json.ListLocationConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Intialise la base de donnée
 */
@Database(entities = [Calendar::class], version = 5)
@TypeConverters(Converters::class,ListLocationConverter::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun calendarDao(): CalendarDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        /**
         * Récupère une instance de la base de donnée.
         * Si la base de donnée n'existe pas une nouvelle est créée
         *
         * @param context
         * @param scope
         * @return AppDatabase
         */
        fun getDatabase(context: Context, scope: CoroutineScope): AppDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "calendar_database"
                ).fallbackToDestructiveMigration().addCallback(AppDatabaseCallback(scope)).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    /**
     * Callback appelée à la création de la base de données
     *
     * @property scope
     */
    private class AppDatabaseCallback(val scope: CoroutineScope) : RoomDatabase.Callback() {
        /**
         * Override the onCreate method to populate the database.
         */
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            // If you want to keep the data through app restarts,
            // comment out the following line.
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    populateDatabase(database.calendarDao())
                }
            }
        }

        /**
         * Ajoute des données après la création de la base de données
         *
         * @param calendarDao
         */
        suspend fun populateDatabase(calendarDao: CalendarDao) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            calendarDao.deleteAll()
            var contact = Contact("gmaze@enssat.fr", "0438560912", "gurvan")
            var location = arrayListOf<Location>(Location(1, "Télétravail"),Location(2, "Présentiel"),Location(3, "Télétravail"),Location(4, "Télétravail"),Location(5, "Télétravail"))
            val calendar = Calendar("Gurvan Mazé","https://picsum.photos/id/1035/800/800",43,location,contact)
            calendarDao.insert(calendar)

            contact = Contact("cfray@enssat.fr", "0478906532", "cyrille")
            location = arrayListOf<Location>(Location(1, "Télétravail"),Location(2, "Télétravail"),Location(3, "Présentiel"),Location(4, "Télétravail"),Location(5, "Télétravail"))
            val calendar2 = Calendar("Cyrille Fraysse","https://picsum.photos/id/238/800/800",89,location,contact)
            calendarDao.insert(calendar2)
        }
    }
}