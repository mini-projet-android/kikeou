package fr.enssat.kikeou.maze_fraysse.calendar.create

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import fr.enssat.kikeou.maze_fraysse.R
import fr.enssat.kikeou.maze_fraysse.main.MainViewModel
import fr.enssat.kikeou.maze_fraysse.model.Calendar
import fr.enssat.kikeou.maze_fraysse.model.Contact
import fr.enssat.kikeou.maze_fraysse.model.Location
import java.util.concurrent.Executors

/**
 * Fragment qui contient un formulaire de création d'un agenda
 *
 */
class CalendarCreationFragment : Fragment() {
    val TAG = "CalendarCreationFragment"

    companion object {
        fun newInstance() = CalendarCreationFragment()
    }

    private lateinit var newCalendar: Calendar
    private val _mainViewModel: MainViewModel by activityViewModels()

    /**
     * A la création de la vue, instanciation du fichier de mise en page avec l'inflater
     *@param
     *@return inflater
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.calendar_creation_fragment, container, false)
    }

    /**
     * Une fois la vue créé, création des différents éléments de la page : champs de texte et boutons
     *@param view: View
     *@param  savedInstanceState: Bundle
     *@return
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val buttonCreate = view.findViewById<Button>(R.id.sendForm)

        this.newCalendar = Calendar(null,null,null,null,null)
        var locations: ArrayList<Location> = ArrayList<Location>()

        val buttonSelectPhoto = view.findViewById<Button>(R.id.buttonPhotoUrl)
        buttonSelectPhoto.setOnClickListener{
            var url:String = view.findViewById<EditText>(R.id.photoUrl).text.toString()
            getImageFromUrl(url)
        }

        // Id of edit text of each day
        var idEditTexts :ArrayList<Int> = ArrayList<Int>()
        idEditTexts.add(R.id.mondaySpinner);
        idEditTexts.add(R.id.tuesdaySpinner);
        idEditTexts.add(R.id.wednesdaySpinner);
        idEditTexts.add(R.id.thursdaySpinner);
        idEditTexts.add(R.id.fridaySpinner);

        buttonCreate.setOnClickListener{
            newCalendar.name = view.findViewById<EditText>(R.id.NameInput).text.toString()
            newCalendar.week = view.findViewById<EditText>(R.id.WeekInput).text.toString().toInt()
            newCalendar.photo = view.findViewById<EditText>(R.id.photoUrl).text.toString()

            newCalendar.contact = Contact(view.findViewById<EditText>(R.id.editTextEmail).text.toString(),view.findViewById<EditText>(R.id.editTextPhone).text.toString(),view.findViewById<EditText>(R.id.editFb).text.toString())

            var currentDay=1
            for(idEditText in idEditTexts){
                var editTextDay = view.findViewById<EditText>(idEditText)
                locations.add(Location(currentDay,editTextDay.text.toString()))
                currentDay++
            }
            newCalendar.loc = locations

            Log.d(TAG,"New calendar name : ${newCalendar.name!!}")
            Log.d(TAG,"New calendar week : ${newCalendar.week!!}")
            _mainViewModel.insertCalendar(newCalendar)
            view.findNavController().navigate(R.id.GoToMainFragment)
        }
    }

    /**
     * Récupère une image à partir d'une url
     *@param  imageURL:String
     *@return
     */
    fun getImageFromUrl(imageURL:String){
        // Get image view
        val imageView = view?.findViewById<ImageView>(R.id.newCalendarImage)

        val executor = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        // Image to display
        var image: Bitmap? = null

        // Only for Background process (can take time depending on the Internet speed)
        executor.execute {

            // Tries to get the image and post it in the ImageView
            // with the help of Handler
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)

                // Only for making changes in UI
                handler.post {
                    imageView?.setImageBitmap(image)
                }
            }

            // If the URL doesnot point to
            // image or any other kind of failure
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}