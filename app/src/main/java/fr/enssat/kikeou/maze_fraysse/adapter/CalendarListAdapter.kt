package fr.enssat.kikeou.maze_fraysse.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.maze_fraysse.main.CellClickListener
import fr.enssat.kikeou.maze_fraysse.model.Calendar

/**
 * Adapter à la recycler view manipuler dans le main fragment
 *
 * @property cellClickListener Listener pour gérer le click sur un item du recycler view
 */
class CalendarListAdapter(private val cellClickListener: CellClickListener) : ListAdapter<Calendar, CalendarViewHolder>(CallendarDiff()) {

    /**
     * Créer un view holder
     *
     * @param parent
     * @param viewType
     * @return CalendarViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        return CalendarViewHolder.create(parent)
    }

    /**
     * Configure l'afficher d'un item en utilisant CalendarViewHolder
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        val current: Calendar? = getItem(position)
        holder.itemView.setOnClickListener{ current?.getId()?.let { id ->
            cellClickListener.onCellClickListener(
                id,
                position
            )
        } }
        holder.bind(current?.name + " / " + current?.week)
    }

    /**
     * Compare des items
     *
     */
    internal class CallendarDiff : DiffUtil.ItemCallback<Calendar>() {
        override fun areItemsTheSame(oldItem: Calendar, newItem: Calendar): Boolean {
            return oldItem === newItem
        }
        override fun areContentsTheSame(oldItem: Calendar, newItem: Calendar): Boolean {
            return oldItem.name.equals(newItem.name)
        }
    }
}