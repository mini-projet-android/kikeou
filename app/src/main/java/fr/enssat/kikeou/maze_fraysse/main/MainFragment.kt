package fr.enssat.kikeou.maze_fraysse.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SearchView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.maze_fraysse.R
import fr.enssat.kikeou.maze_fraysse.adapter.CalendarListAdapter
import fr.enssat.kikeou.maze_fraysse.model.Calendar

/**
 * Fragment principale de l'application. Il correspond à la page d'accueil de l'application
 *
 */
class MainFragment : Fragment(), CellClickListener {
    val TAG = "MainFragment"
    companion object {
        fun newInstance() = MainFragment()
    }

    private val _mainViewModel: MainViewModel by activityViewModels()

    /**
     * A la création de la vue, instanciation du fichier de mise en page avec l'inflater
     * @param
     * @return inflater
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    /**
     * Une fois la vue créé, création des différents éléments de la page : champs de texte et boutons
     * @param view: View
     * @param  savedInstanceState: Bundle
     * @return
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.findViewById<Button>(R.id.ButtonCalendarCreation).setOnClickListener {
            view.findNavController().navigate(R.id.GoToCalendarCreation)
        }
        view.findViewById<Button>(R.id.ButtonScanQRcode).setOnClickListener {
            view.findNavController().navigate(R.id.GoToQrcodeRead)
        }
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = CalendarListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)

        // Observer to update recyclerview on change on calendars database
        _mainViewModel.calendars.observe(viewLifecycleOwner) { calendars ->
            adapter.submitList(calendars)
        }
        val searchView = view.findViewById<SearchView>(R.id.searchview)
        // Manage search of a calendar
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean { return true }

            override fun onQueryTextChange(newText: String?): Boolean {
                var list:List<Calendar> = adapter.currentList
                var newList:ArrayList<Calendar> = ArrayList<Calendar>()
                if(!newText?.isEmpty()!!) {
                    for (item: Calendar in list) {
                        if (newText?.let { item.name?.contains(it) } == true) {
                            newList.add(item)
                        }
                    }
                    adapter.submitList(newList)
                }else{
                    adapter.submitList(_mainViewModel.calendars.value)
                }
                return true
            }
        })
    }

    /**
     * Listener. Renvoie vers le fragment de lecture d'un calendrier correspondant à la cellule sur laquelle on a cliqué
     * @param position: Int
     * @param  idCalendar: Int
     */
    override fun onCellClickListener(idCalendar: Int,position: Int) {
        val bundle = bundleOf("idCalendar" to position)
        view?.findNavController()?.navigate(R.id.calendarReadFragment,bundle);
    }
}

interface CellClickListener {
    fun onCellClickListener(idCalendar: Int,position: Int)
}