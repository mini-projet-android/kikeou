package fr.enssat.kikeou.maze_fraysse.model.json

@Target(AnnotationTarget.TYPE,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FUNCTION
)
@Retention(AnnotationRetention.RUNTIME)
annotation class CalendarJsonAnnotation {
}