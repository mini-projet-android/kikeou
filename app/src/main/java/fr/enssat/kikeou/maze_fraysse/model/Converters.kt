package fr.enssat.kikeou.maze_fraysse.model

import android.net.Uri
import androidx.room.TypeConverter

/**
 * Convertisseur associer à une URI
 *
 */
class Converters {
    /**
     * Convertit une uri en String
     *
     * @param value
     * @return Uri
     */
    @TypeConverter
    fun fromString(value: String?): Uri? {
        return Uri.parse(value)
    }

    /**
     * Convertit un String en Uri
     *
     * @param uri
     * @return String
     */
    @TypeConverter
    fun uriToString(uri: Uri?): String {
        return uri.toString()
    }
}