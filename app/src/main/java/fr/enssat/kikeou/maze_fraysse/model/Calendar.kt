package fr.enssat.kikeou.maze_fraysse.model

import android.net.Uri
import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.maze_fraysse.model.json.ListLocationConverter

/**
 * Classe qui représente une localisation
 *
 * @property day
 * @property place
 */
@JsonClass(generateAdapter = true)
data class Location(var day:Int, var place:String)

/**
 * Classe qui représente un contact
 *
 * @property mail
 * @property tel
 * @property fb
 */
@JsonClass(generateAdapter = true)
data class Contact(var mail:String, var tel:String?, var fb:String?)

/**
 * Classe qui représente un agenda
 *
 * @property name
 * @property photo
 * @property week
 * @property loc
 * @property contact
 */
@Entity(tableName = "calendar_table")
@JsonClass(generateAdapter = true)
data class Calendar(
    @ColumnInfo(name = "name")
    @field:Json(name = "name") var name: String?,

    @ColumnInfo(name = "photo")
    @field:Json(name = "photo") var photo: String?,

    @ColumnInfo(name = "week")
    @field:Json(name = "week") var week: Int?,

    @TypeConverters(ListLocationConverter::class)
    @field:Json(name = "loc") var loc: List<Location>?,

    @Embedded
    @field:Json(name = "contact") var contact:Contact?){

    @ColumnInfo(name="id")
    @PrimaryKey(autoGenerate = true)
    @Transient
    private var id:Int = 0
    fun getId():Int {
            return id
        }
        fun  setId(value:Int){
            id = value
        }
    }
