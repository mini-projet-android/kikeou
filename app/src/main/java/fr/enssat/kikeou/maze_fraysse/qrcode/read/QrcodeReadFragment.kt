package fr.enssat.kikeou.maze_fraysse.qrcode.read

import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.camera.core.AspectRatio
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.objects.ObjectDetection
import com.google.mlkit.vision.objects.ObjectDetector
import com.google.mlkit.vision.objects.custom.CustomObjectDetectorOptions
import fr.enssat.kikeou.maze_fraysse.R
import fr.enssat.kikeou.maze_fraysse.databinding.QrcodeReadFragmentBinding
import fr.enssat.kikeou.maze_fraysse.main.MainViewModel
import fr.enssat.kikeou.maze_fraysse.model.Calendar
import fr.enssat.kikeou.maze_fraysse.model.Location
import fr.enssat.kikeou.maze_fraysse.model.json.CalendarJsonParser

import fr.enssat.kikeou.maze_fraysse.qrcode.read.QrcodeReadFragment
import fr.enssat.kikeou.maze_fraysse.qrcode.read.QrcodeReadViewModel
import net.glxn.qrgen.android.QRCode
import org.json.JSONObject
import java.util.zip.Deflater

/**
 * Fragment qui lit un Qrcode en utilisant la caméra
 *
 */
class QrcodeReadFragment : Fragment() {
    private val REQUEST_CODE = 123456

    private lateinit var binding: QrcodeReadFragmentBinding
    private lateinit var objectDetector: ObjectDetector
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>
    private var findQRCode : Boolean = false
    private val _mainViewModel: MainViewModel by activityViewModels()

    companion object {
        fun newInstance() = QrcodeReadFragment()

    }

    private lateinit var viewModel: QrcodeReadViewModel

    /**
     * A la création de la vue, instanciation du fichier de mise en page avec l'inflater.
     * Besoin d'un binding pour l'utilisation de la caméra
     *@param
     *@return binding
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =  QrcodeReadFragmentBinding.inflate(layoutInflater)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){

    }

    /**
     * Traitement de la réponse de demande de permission d'utiliser la caméra. Si permission : initialisation de la caméra et traitement
     *@param requestCode: Int
     *@param  permissions: Array<String>
     *@param grantResults: IntArray
     *@return
     */
    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        when (requestCode) {
            REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    initCameraAndTF()
                } else {
                   // Toast.makeText(this.requireContext(), this.getString(androidx.camera.core.R.string.permissionToGrant), Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }
    override fun onResume() {
        super.onResume()
        //Do not forget to declare  <uses-permission android:name="android.permission.CAMERA"/> in your manifest
        if (ContextCompat.checkSelfPermission(this.requireContext(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initCameraAndTF()
        } else {
            val permissions = arrayOf(android.Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(this.requireActivity(),permissions,REQUEST_CODE)
        }
    }
    //Only the original thread that created a view can touch its views.

    /**
     * Initialisation de la caméra affichage de la preview sur l'écran.
     * Si mlkit reconnaît un qrcode, on récupère les infos du QR code.
     * Si ce QRcode contient un dictionnaire, on quitte ce fragment et on affiche le fragment de lecture de dictionnaire
     *@return
     */
    private fun initCameraAndTF(){
        val TFModel = LocalModel.Builder()
            .setAssetFilePath("lite-model_object_detection_mobile_object_labeler_v1_1.tflite")
            .build()
        val customObjectDetectorOptions =
            CustomObjectDetectorOptions.Builder(TFModel)
                .setDetectorMode(CustomObjectDetectorOptions.STREAM_MODE)
                .enableClassification()
                .setClassificationConfidenceThreshold(0.5f)
                .setMaxPerObjectLabelCount(3)
                .build()
        objectDetector = ObjectDetection.getClient(customObjectDetectorOptions)

        val barcodeScanner: BarcodeScanner = BarcodeScanning.getClient(
            BarcodeScannerOptions.Builder()
                .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                .build()
        )

        findQRCode = false
        cameraProviderFuture = ProcessCameraProvider.getInstance(this.requireContext())
        cameraProviderFuture.addListener( {

            val cameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .build()
            val imageAnalysis = ImageAnalysis.Builder()
                .setTargetResolution(Size(1080, 2340))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()

            imageAnalysis.setAnalyzer(
                ContextCompat.getMainExecutor(this.requireContext()),
                @androidx.camera.core.ExperimentalGetImage { imageProxy ->
                    val rotationDegrees = imageProxy.imageInfo.rotationDegrees
                    val image = imageProxy.image
                    if(image != null && findQRCode == false) {
                        val processImage = InputImage.fromMediaImage(image, rotationDegrees)
                        val newCalendar = barcodeScanner.process(processImage).addOnFailureListener {
                            Log.e("ScannerActivity", "Error: $it.message")
                            imageProxy.close()
                        }.addOnSuccessListener { barcodes ->
                            for (barcode in barcodes) {
                                val bounds = barcode.boundingBox
                                val corners = barcode.cornerPoints

                                val rawValue = barcode.rawValue
                                val valueType = barcode.valueType

                                // Get json from qrcode and create calendar
                                val newCalendar = CalendarJsonParser.parseCalendar(rawValue)
                                if(newCalendar != null){
                                    findQRCode = true
                                    // Add calendar in view model and go to main fragement
                                    _mainViewModel.insertCalendar(newCalendar)
                                    view?.findNavController()?.navigate(R.id.GoToMainFragmentFromReadQrcode);
                                }
                            }
                            imageProxy.close()
                        }
                    }
                })

            // Create a new camera selector each time, enforcing lens facing
            val cameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()

            // Apply declared configs to CameraX using the same lifecycle owner
            cameraProvider.unbindAll()
            val camera = cameraProvider.bindToLifecycle(
                this as LifecycleOwner, cameraSelector, preview, imageAnalysis)

            // Use the camera object to link our preview use case with the view
            preview.setSurfaceProvider(binding.previewView.surfaceProvider)
        },
            ContextCompat.getMainExecutor(this.requireContext())
        )
    }

    /**
     * Dessine un carré sur l'écran
     *
     * @property rect
     * @property text
     * @constructor
     *
     * @param context
     */
    private class Draw(context: Context?, var rect: Rect, var text: String) : View(context) {
        lateinit var paint: Paint
        lateinit var textPaint: Paint

        init {
            init()
        }

        private fun init() {
            paint = Paint()
            paint.color = Color.RED
            paint.strokeWidth = 20f
            paint.style = Paint.Style.STROKE

            textPaint = Paint()
            textPaint.color = Color.RED
            textPaint.style = Paint.Style.FILL
            textPaint.textSize = 80f
        }

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            canvas.drawText(text, rect.centerX().toFloat(), rect.centerY().toFloat(), textPaint)
            canvas.drawRect(
                rect.left.toFloat(),
                rect.top.toFloat(),
                rect.right.toFloat(),
                rect.bottom.toFloat(),
                paint
            )
        }
    }
    fun hideKeyboard() {
        val view = this.view?.findFocus()
        if (view != null) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(QrcodeReadViewModel::class.java)
        // TODO: Use the ViewModel
    }
}