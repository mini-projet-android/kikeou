package fr.enssat.kikeou.maze_fraysse.model.json

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import fr.enssat.kikeou.maze_fraysse.model.Location

/**
 * Convertisseur associé à une liste de localisation
 *
 */
class ListLocationConverter {
    private val moshi = Moshi.Builder().build()
    private val locType = Types.newParameterizedType(List::class.java, Location::class.java)
    private val locAdapter = moshi.adapter<List<Location>>(locType)

    /**
     * Convertit une json en une liste de localisation
     *
     * @param string
     * @return List<Location>
     */
    @TypeConverter
    fun stringToLocations(string: String): List<Location> {
        return locAdapter.fromJson(string).orEmpty()
    }

    /**
     * Convertit une liste de localisation en json
     *
     * @param loc
     * @return String
     */
    @TypeConverter
    fun LocationsToString(loc: List<Location>): String {
        return locAdapter.toJson(loc)
    }
}