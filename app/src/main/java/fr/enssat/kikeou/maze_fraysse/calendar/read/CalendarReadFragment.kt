package fr.enssat.kikeou.maze_fraysse.calendar.read

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.NOT_FOCUSABLE
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import fr.enssat.kikeou.maze_fraysse.R
import fr.enssat.kikeou.maze_fraysse.calendar.create.CalendarCreationFragment
import fr.enssat.kikeou.maze_fraysse.main.MainViewModel
import fr.enssat.kikeou.maze_fraysse.model.Calendar
import fr.enssat.kikeou.maze_fraysse.model.json.CalendarJsonParser
import net.glxn.qrgen.android.QRCode
import org.json.JSONObject
import java.util.concurrent.Executors
import java.util.zip.Deflater

/**
 * Fragment qui contient permet de lire le contenu d'un agenda
 *
 */
class CalendarReadFragment : Fragment() {

    val TAG = "CalendarReadFragment"

    companion object {
        fun newInstance() = CalendarReadFragment()
    }

    private val _mainViewModel: MainViewModel by activityViewModels()

    /**
     * A la création de la vue, instanciation du fichier de mise en page avec l'inflater
     *@param
     *@return inflater
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.calendar_read_fragment, container, false)
    }

    /**
     * Une fois la vue créé, création des différents éléments de la page : champs de texte et boutons
     *@param view: View
     *@param  savedInstanceState: Bundle
     *@return
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Get calendar from view model
        val idCalendar = arguments?.getInt("idCalendar")
        val calendar = _mainViewModel.calendars.value?.get(idCalendar!!)

        // Display calendar attributes
        val nameView = view.findViewById<TextView>(R.id.NameInput)
        nameView.text = calendar?.name

        val phoneView = view.findViewById<TextView>(R.id.editTextPhone)
        phoneView.text = calendar?.contact?.tel
        phoneView.focusable = NOT_FOCUSABLE

        val emailView = view.findViewById<TextView>(R.id.editTextEmail)
        emailView.text = calendar?.contact?.mail
        emailView.focusable = NOT_FOCUSABLE

        val fbView = view.findViewById<TextView>(R.id.editFb)
        fbView.text = calendar?.contact?.fb
        fbView.focusable = NOT_FOCUSABLE

        getImageFromUrl(calendar?.photo.toString())

        val weekView = view.findViewById<TextView>(R.id.WeekInput)
        weekView.text = calendar?.week.toString()

        // Id of text view of each day
        val idTextViews :ArrayList<Int> = ArrayList<Int>()
        idTextViews.add(R.id.mondaySpinner);
        idTextViews.add(R.id.tuesdaySpinner);
        idTextViews.add(R.id.wednesdaySpinner);
        idTextViews.add(R.id.thursdaySpinner);
        idTextViews.add(R.id.fridaySpinner);

        for(location in calendar?.loc!!){
            var day:Int = location.day
            var place = location.place

            // Get text view of the day
            var editTextDay = view.findViewById<TextView>(idTextViews.get(day-1))
            editTextDay.text = place
            editTextDay.focusable = NOT_FOCUSABLE

        }

        val imageQrcode = view.findViewById<ImageView>(R.id.qrcodeView)
        val buttonCreate = view.findViewById<Button>(R.id.qrcodeCreation)
        buttonCreate.setOnClickListener {
            val json = CalendarJsonParser.parseJson(calendar)

            /*
            * Generate bitmap from the text provided,
            * The QR code can be saved using other methods such as stream(), file(), to() etc.
            * */
            val bitmap = QRCode.from(json).withCharset("UTF-8").withSize(1000, 1000).bitmap()
            imageQrcode.setImageBitmap(bitmap)
        }

        val buttonDelete = view.findViewById<Button>(R.id.deleteCalendar)
        buttonDelete.setOnClickListener{
            _mainViewModel.deleteCalendar(calendar?.getId())
            view?.findNavController()?.navigate(R.id.action_calendarReadFragment_to_mainFragment);
        }

        val buttonUpdate = view.findViewById<Button>(R.id.editCalendar);
        buttonUpdate.setOnClickListener{
            val bundle = bundleOf("idCalendar" to idCalendar)
            view?.findNavController()?.navigate(R.id.action_calendarReadFragment_to_calendarUpdateFragment,bundle);
        }
    }

    /**
     * Récupère une image à partir d'une url
     *@param  imageURL:String
     *@return
     */
    fun getImageFromUrl(imageURL:String){
        // Get image view
        val imageView = view?.findViewById<ImageView>(R.id.photoView)

        val executor = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        // Image to display
        var image: Bitmap? = null

        // Only for Background process (can take time depending on the Internet speed)
        executor.execute {

            // Tries to get the image and post it in the ImageView
            // with the help of Handler
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)

                // Only for making changes in UI
                handler.post {
                    view?.findViewById<RelativeLayout>(R.id.loadingPanel)?.setVisibility(View.GONE);
                    imageView?.setImageBitmap(image)
                }
            }

            // If the URL doesnot point to
            // image or any other kind of failure
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}