package fr.enssat.kikeou.maze_fraysse

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import fr.enssat.kikeou.maze_fraysse.main.MainViewModel
import fr.enssat.kikeou.maze_fraysse.main.MainViewModelFactory
import fr.enssat.kikeou.maze_fraysse.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

/**
 * Activité principale de l'application.
 * Elle affiche par défaut le fragment MainFragment
 */
class MainActivity : AppCompatActivity() {

    private lateinit var _mainViewModel: MainViewModel
    private lateinit var appBarConfiguration: AppBarConfiguration

    /**
     * Initialisation de l'activité
     *
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val scope = CoroutineScope(SupervisorJob())
        val database = AppDatabase.getDatabase(this,scope)
        val repository = CalendarRepository(database.calendarDao())

        // Get a new or existing ViewModel using Agenda view model factory.
        _mainViewModel = ViewModelProvider(this, MainViewModelFactory(repository)).get(
            MainViewModel::class.java)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        val navController = navHostFragment.navController
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        appBarConfiguration = AppBarConfiguration(navController.graph)
        toolbar.setupWithNavController(navController, appBarConfiguration)

    }
}