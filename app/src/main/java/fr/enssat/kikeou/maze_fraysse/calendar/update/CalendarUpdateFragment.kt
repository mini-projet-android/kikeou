package fr.enssat.kikeou.maze_fraysse.calendar.update

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import fr.enssat.kikeou.maze_fraysse.R
import fr.enssat.kikeou.maze_fraysse.calendar.read.CalendarReadFragment
import fr.enssat.kikeou.maze_fraysse.main.MainViewModel
import fr.enssat.kikeou.maze_fraysse.model.Calendar
import fr.enssat.kikeou.maze_fraysse.model.Contact
import fr.enssat.kikeou.maze_fraysse.model.Location
import java.util.concurrent.Executors

/**
 * Fragment qui contient un formulaire de création d'un agenda
 *
 */
class CalendarUpdateFragment : Fragment() {
    val TAG = "CalendarUpdateFragment"

    companion object {
        fun newInstance() = CalendarReadFragment()
    }

    private val _mainViewModel: MainViewModel by activityViewModels()

    /**
     * A la création de la vue, instanciation du fichier de mise en page avec l'inflater
     *@param
     *@return inflater
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.calendar_update_fragment, container, false)
    }

    /**
     * Une fois la vue créé, création des différents éléments de la page : champs de texte et boutons
     *@param view: View
     *@param  savedInstanceState: Bundle
     *@return
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Get calendar from view model
        val idCalendar = arguments?.getInt("idCalendar")
        val calendar = _mainViewModel.calendars.value?.get(idCalendar!!)
        var locations: ArrayList<Location> = ArrayList<Location>()

        // Display calendar attributes
        val nameView = view.findViewById<TextView>(R.id.NameInput)
        nameView.text = calendar?.name

        val phoneView = view.findViewById<TextView>(R.id.editTextPhone)
        phoneView.text = calendar?.contact?.tel

        val emailView = view.findViewById<TextView>(R.id.editTextEmail)
        emailView.text = calendar?.contact?.mail

        val photoUrl = view.findViewById<TextView>(R.id.photoUrl)
        photoUrl.text = calendar?.photo

        val buttonSelectPhoto = view.findViewById<Button>(R.id.buttonPhotoUrl)
        buttonSelectPhoto.setOnClickListener{
            var url:String = view.findViewById<EditText>(R.id.photoUrl).text.toString()
            getImageFromUrl(url)
        }

        val fbView = view.findViewById<TextView>(R.id.editFb)
        fbView.text = calendar?.contact?.fb

        getImageFromUrl(calendar?.photo.toString())

        val weekView = view.findViewById<TextView>(R.id.WeekInput)
        weekView.text = calendar?.week.toString()

        // Id of text view of each day
        var idEditTexts :ArrayList<Int> = ArrayList<Int>()
        idEditTexts.add(R.id.mondaySpinner);
        idEditTexts.add(R.id.tuesdaySpinner);
        idEditTexts.add(R.id.wednesdaySpinner);
        idEditTexts.add(R.id.thursdaySpinner);
        idEditTexts.add(R.id.fridaySpinner);

        for(location in calendar?.loc!!){
            var day:Int = location.day
            var place = location.place

            // Get text view of the day
            var editTextDay = view.findViewById<TextView>(idEditTexts.get(day-1))
            editTextDay.text = place

        }

        val updateButton = view.findViewById<Button>(R.id.sendForm)
        updateButton.setOnClickListener{
            calendar.name = view.findViewById<EditText>(R.id.NameInput).text.toString()
            calendar.week = view.findViewById<EditText>(R.id.WeekInput).text.toString().toInt()
            calendar.photo = view.findViewById<EditText>(R.id.photoUrl).text.toString()

            calendar.contact = Contact(view.findViewById<EditText>(R.id.editTextEmail).text.toString(),view.findViewById<EditText>(R.id.editTextPhone).text.toString(),view.findViewById<EditText>(R.id.editFb).text.toString())

            locations = ArrayList<Location>()
            var currentDay=1
            for(idEditText in idEditTexts){
                var editTextDay = view.findViewById<EditText>(idEditText)
                locations.add(Location(currentDay,editTextDay.text.toString()))
                currentDay++
            }
            calendar.loc = locations

            _mainViewModel.updateCalendar(calendar)
            view?.findNavController()?.navigate(R.id.action_calendarUpdateFragment_to_mainFragment);
        }
    }

    /**
     * Récupère une image à partir d'une url
     *@param  imageURL:String
     *@return
     */
    fun getImageFromUrl(imageURL:String) {
        // Get image view
        val imageView = view?.findViewById<ImageView>(R.id.newCalendarImage)

        val executor = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        // Image to display
        var image: Bitmap? = null

        // Only for Background process (can take time depending on the Internet speed)
        executor.execute {

            // Tries to get the image and post it in the ImageView
            // with the help of Handler
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)

                // Only for making changes in UI
                handler.post {
                    view?.findViewById<RelativeLayout>(R.id.loadingPanel)?.setVisibility(View.GONE);
                    imageView?.setImageBitmap(image)
                }
            }

            // If the URL doesnot point to
            // image or any other kind of failure
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}